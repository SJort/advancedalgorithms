\babel@toc {dutch}{}
\contentsline {section}{\numberline {1}Inleiding}{2}% 
\contentsline {section}{\numberline {2}Requirements en specificaties}{2}% 
\contentsline {subsection}{\numberline {2.1}Requirements}{3}% 
\contentsline {section}{\numberline {3}Modellen}{3}% 
\contentsline {subsection}{\numberline {3.1}De Kripke structuur}{3}% 
\contentsline {subsection}{\numberline {3.2}Soorten modellen}{3}% 
\contentsline {subsection}{\numberline {3.3}Tijd}{3}% 
\contentsline {subsection}{\numberline {3.4}Guards en invarianten}{3}% 
\contentsline {subsection}{\numberline {3.5}Deadlock}{3}% 
\contentsline {subsection}{\numberline {3.6}Zeno gedrag}{4}% 
\contentsline {section}{\numberline {4}Logica}{4}% 
\contentsline {subsection}{\numberline {4.1}Propositielogica}{4}% 
\contentsline {subsection}{\numberline {4.2}Predicatenlogica}{4}% 
\contentsline {subsection}{\numberline {4.3}Kwantoren}{5}% 
\contentsline {subsection}{\numberline {4.4}Dualiteiten}{5}% 
\contentsline {section}{\numberline {5}Computation tree logic}{5}% 
\contentsline {subsection}{\numberline {5.1}De computation tree}{5}% 
\contentsline {subsection}{\numberline {5.2}Operator: AG}{5}% 
\contentsline {subsection}{\numberline {5.3}Operator: EG}{5}% 
\contentsline {subsection}{\numberline {5.4}Operator: AF}{5}% 
\contentsline {subsection}{\numberline {5.5}Operator: EF}{5}% 
\contentsline {subsection}{\numberline {5.6}Operator: AX}{5}% 
\contentsline {subsection}{\numberline {5.7}Operator: EX}{5}% 
\contentsline {subsection}{\numberline {5.8}Operator: p U q}{5}% 
\contentsline {subsection}{\numberline {5.9}Operator: p R q}{5}% 
\contentsline {subsection}{\numberline {5.10}Fairness}{5}% 
\contentsline {subsection}{\numberline {5.11}Liveness}{5}% 
