# Fix font
On Arch Linux, the font in uppaal is not anti aliased by default.  
Install xsettingsd:
```shell
sudo pacman -S xsettingsd
```
Edit the xsettingd config file:
```shell
vim .xsettingsd
```
And put the following in the file:
```properties
Xft/Hinting 1
Xft/HintStyle "hintslight"
Xft/Antialias 1
Xft/RGBA "rgb"
```
Run xsettingsd at startup of your system, so put it from your .xinitrc for example.

# Start Uppaal
To start Uppaal, run:
```shell
./uppaal &
```

# Models
In the models folder you can find some sample models.  
If it is in a folder, each file resembles a version.
