# Advanced Algorithms

Advanced Algorithms is a minor I am following.  
[Website professor](https://med.hr.nl/oelew/verificatie.html)

# Frequently used information
Git force pull:
```shell
git fetch --all
git reset --hard origin/master
```

# Setup LaTeX for Arch Linux
### Setup .vimrc
```shell
vim ~/.vimrc
```
Append to top of .vimrc:
```viml
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin()
Plug 'lervag/vimtex'
call plug#end()
let g:tex_flavor = 'latex'
let g:vimtex_view_method = 'zathura'
set packpath+=~/.vim/pack/
```

### Install packages
```shell
sudo pacman -S zathura zathura-pdf-mupdf texlive-most xdotool
```

### Run
Type in normal mode in vim in a .tex file:
```viml
\ll
```
This opens the compiled .tex file in Zathura. It updates when changes are written.

